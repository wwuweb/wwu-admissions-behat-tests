#features/anonymous/uptime.feature

Feature: Uptime check
  Double check the front page to see if the site is currently online

  Scenario: Front page should return status code 200
    When I visit "/"
    Then the response status code should be 200

  Scenario: Test to see the Applyweb output contains the date followed on page with dates
    When I visit "/calendar"
    Then I should see an event date
  
  @javascript
  Scenario: Check to see if the PSAT Autosubmit Javascript is functioning properly.
    When I visit "/forms/psat-autofill?id=A12345678&Search_ID=A12345678&email_address=just.testing%40wwu.edu&first_name=JustTesting" 
    Then I should see "Thanks for signing up - we'll be in touch soon!"
