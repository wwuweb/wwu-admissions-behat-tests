@javascript
Feature: Testing social media panel display

  Background:
    When I visit "/"

  Scenario: Facebook display
    When I press "Facebook feed"
    And I wait 1 seconds
    Then I should see an element with the selector "#FB-posts"
    And I should see an element with the selector "#FB-posts .social-media-feed-message"
    And I should see an element with the selector "#FB-posts .western-slider-control-next"
    And I should see an element with the selector "#FB-posts .western-slider-control-prev"
    But I should not see an element with the selector "#TW-posts"
    And I should not see an element with the selector "#IG-posts"

  Scenario: Twitter display
    When I press "Twitter feed"
    And I wait 1 seconds
    Then I should see an element with the selector "#TW-posts"
    And I should see an element with the selector "#TW-posts .social-media-feed-time"
    And I should see an element with the selector "#TW-posts .western-slider-control-next"
    And I should see an element with the selector "#TW-posts .western-slider-control-prev"
    But I should not see an element with the selector "#FB-posts"
    And I should not see an element with the selector "#IG-posts"

  Scenario: Instagram display
    When I press "Instagram feed"
    And I wait 1 seconds
    Then I should see an element with the selector "#IG-posts"
    And I should see an element with the selector "#IG-posts .social-media-feed-message"
    And I should see an element with the selector "#IG-posts .western-slider-control-next"
    And I should see an element with the selector "#IG-posts .western-slider-control-prev"
    But I should not see an element with the selector "#FB-posts"
    And I should not see an element with the selector "#TW-posts"

  Scenario: Slider controls
    When I press "Facebook feed"
    And I wait 1 seconds
    And I view the social media slider
    And I click on the element with the selector "#FB-posts .western-slider-control-next"
    And I wait 1 seconds
    Then The first visible social media item should change