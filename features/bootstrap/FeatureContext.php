<?php

use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Behat\Exception\PendingException;
use Drupal\DrupalExtension\Context\RawDrupalContext;

class FeatureContext extends RawDrupalContext implements SnippetAcceptingContext {

  /**
   * @Then /^I should see an element with the selector "([^"]*)"$/
   */
  public function assertPageContainsElement($selector) {
    $results = $this->getSession()->getPage()->findAll('css', $selector);
    
    if (!$results || count($results) < 1) {
    	throw new Exception("No element matching selector ${selector} was found.");
    }

	$numVisible = 0;
	foreach ($results as $result) {
      if($result->isVisible()) {
      	$numVisible++;
      	break;
      }
    }
    
    if ($numVisible < 1) {
        throw new Exception("All elements matching ${selector} were hidden.");
    }
  }

  /**
   * @Then /^I should not see an element with the selector "([^"]*)"$/
   */
  public function assertPageNotContainsElement($selector) {
    $result = $this->getSession()->getPage()->find('css', $selector);

    if ($result && $result->isVisible()) {
      throw new Exception("The selector ${selector} was found.");
    }
  }

  /**
   * @Given /^the browser has the dimensions "([^"]*)" by "([^"]*)"$/
   */
  public function setBrowserDimensions($width, $height) {
    $this->getSession()->resizeWindow(intval($width), intval($height));
  }

  /**
   * @When /^I resize the browser to the dimensions "([^"]*)" by "([^"]*)"$/
   */
  public function resizeBrowser($width, $height) {
    $this->getSession()->resizeWindow(intval($width), intval($height));
  }

  /**
   * @When /^(?:|I )click on the element with the selector "([^"]*)"$/
   */
  public function clickOnElement($selector) {
    $element = $this->getSession()->getPage()->find('css', $selector);

    if (!$element) {
      throw new Exception("The selector {$selector} was not found.");
    }

    $element->click();
  }

  /**
   * @Given I focus on the iframe :iframe
   */
  public function focusOnTheIframe($iframe) {
    $this->getSession()->switchToIframe($iframe);
  }

  /**
   * @Given I reset to the base iframe
   */
  public function resetToTheBaseIframe() {
    // When called with NULL, switches to main frame
    $this->getSession()->switchToIframe(); 
  }

  /**
   * @Given I refocus on the iframe :iframe
   */
  public function refocusOnIframe($iframe) {
    $this->resetToTheBaseIframe();
    $this->getSession()->switchToIframe($iframe);
  }

  /**
   * @Given I wait :arg1 seconds 
   */ 
  public function iWaitSeconds($arg1) {
    sleep($arg1);
  }

  /**
   * @Given the :module module is enabled
   */
  public function enableDrupalModule($module) {
    $command = "--backend --yes pm-enable";
    $output = $this->getDriver('drush')->$command($module);

    $errorFormat = "The module %s was not enabled.\nThe error message was:\n\n%s";

    if (strpos((string) $output, "{$module} was enabled successfully.") === false && strpos((string) $output, "{$module} is already enabled.") === false) {
      throw new Exception(sprintf($errorFormat, $module, $output));
    }
  }

  /**
   * @Given the :module module is disabled
   */
  public function disableDrupalModule($module) {
    $command = "--backend --yes pm-disable";
    $output = $this->getDriver('drush')->$command($module);

    $errorFormat = "The module %s was not disabled.\nThe error message was:\n\n%s";

    if (strpos((string) $output, "{$module} was disabled successfully.") === false && strpos((string) $output, "{$module} is already disabled.") === false) {
      throw new Exception(sprintf($errorFormat, $module, $output));
    }
  }

  /**
   * @Given I focus on the iframe at index :index
   */
  public function iFocusOnTheIframeAtIndex($index) {
    // The index comes in from Gherkin as a String, needs to be cast to int to avoid 
    // being interpreted as ASCII by Selenium2
    $index = (int)$index;
    $this->getSession()->switchToIframe($index);
  }

  /**
   * @Given I click on the fake :button button
   */
  public function clickFakeButton($button) {
    // Step definition derived from: http://drupal.org/project/wetkit
    $buttons = $this->getSession()->getDriver()->find("//a[text()='{$button}']");
    $button = array_pop($buttons);

    if (!$button || !$button->isVisible()) {
      throw new Exception("The button {$button} is not visible and could not be clicked.");
    }

    $button->click();
  }

}